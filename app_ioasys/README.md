# Desafio ioasys

### Instruções de Instalação (Android)

Clone o projeto
```code
git clone git@bitbucket.org:AdonaiPinheiro/desafio-ioasys.git
```

Execute o comando "yarn" dentro da pasta "app_ioasys", que está dentro do repositório clonado
```code
cd app_ioasys && yarn
```

Utilize o "react-native link" para linkagem das libs
```code
react-native link
```

Agora execute o comando "npx jetify", para que todas as incompatibilidades com o Android X sejam resolvidas
```code
npx jetify
```

***

### Dependências

```code
  "@react-native-community/async-storage": "^1.6.1",
  "axios": "^0.19.0",
  "react": "16.8.3",
  "react-native": "0.59.9",
  "react-native-gesture-handler": "^1.4.1",
  "react-native-linear-gradient": "^2.5.6",
  "react-native-reanimated": "^1.2.0",
  "react-native-screens": "^2.0.0-alpha.3",
  "react-native-vector-icons": "^6.6.0",
  "react-navigation": "^4.0.2",
  "react-navigation-drawer": "^2.1.1",
  "react-navigation-stack": "^1.5.4",
  "react-redux": "^7.1.1",
  "redux": "^4.0.4",
  "redux-thunk": "^2.3.0"
```
### Motivações
- Optei por utilizar o AsyncStorage que, por mais que seja uma lib que passou para a comunidade (ainda bem!), ainda se mostra muito simples e eficiente para armazenamento de dados no dispositivo.

- Axios para fazer as requisições, já que disponibiliza uma boa gama de funcionalidades no seu core e a implementação é simples.

- Gesture-handler, reanimated, screens, navigator, drawer e stack, ao intalar a navegação, utilizei a "react-navigation", na versão 4.^. Trazendo com ela a "gesture-handler, reanimated e screens" como dependências. Acho a instalação e estrutura muito simples e de fácil leitura, além de que outras libs de navegação utilizam ela no core, como por exemplo a "router-flux".

- redux, react-redux e redux-thunk, foram as libs escolhidas para fazer o gerenciamento de estados desta aplicação. Utilizo o redux-thunk para meu middleWare.

- vector-icons, uma lib que oferece uma diversidade enorme de svgs de graça para utilizarmos nos nossos app, facilitando e  muito o trabalho, além de melhorar a aparência dos nossos app

- linear-gradient, gosto desta lib por proporcionar a criação de "Views" com backgrounds em degradê, sem que precise criar imagens PNG, ou utilizar de qualquer outro artifício para criar a sensação de degradê no app
***

### Dev Dependências

```code
  "jetifier": "^1.6.4"
```
### Motivações

- Como estou utilizando a versão 0.59.^ no app, tenho algumas incompatibilidades com o Adroid X, na hora de linkagem e conversão do JS pelo bundler, a jetifier como uma dev dep, com mais algumas configurações faz esse trabalho para mim, arrumando assim toda incompatibilidade de libs.
***

#### Observações:
Apesar do projeto ser "pequeno", julgando apenas pela quantidade de páginas, o nível de complexibilidade é bem alto. Fazendo com que gastemos mais tempo arrumando e corrigindo alguns side-effects. Neste projeto não me preocupei em fazer alterações de ícones ou splash a nível nativo, até pelo meu tempo disponível e pela facilidade em fazê-los, procurei focar na estruturação e desempenho do app.

Gostei bastante do desafio! É sempre bom sair da zona de conforto.