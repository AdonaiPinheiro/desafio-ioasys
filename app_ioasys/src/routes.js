import React from "react";
import { SafeAreaView, ScrollView } from "react-native";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import {
  createDrawerNavigator,
  DrawerNavigatorItems
} from "react-navigation-drawer";
import { createStackNavigator } from "react-navigation-stack";

// Icons
import Icon from "react-native-vector-icons/FontAwesome5";

// Screens
// Switch main screens
import Preload from "./screens/switch/preload";

//Stack Welcome screens
import Welcome from "./screens/switch/welcome";

// Main application inside a drawer
import Dashboard from "./screens/switch/main/dashboard";

// Empresas
import Empresas from "./screens/switch/main/enterprises";
import EnterpriseScreen from "./screens/switch/main/enterprises/enterpriseScreen";

// DrawerHeader
import Header from "./assets/components/drawerHeader";
import Footer from "./assets/components/drawerFooter";

// Custom Drawer
const DrawerContent = props => (
  <SafeAreaView style={{ flex: 1 }}>
    <Header />
    <ScrollView>
      <DrawerNavigatorItems {...props} />
    </ScrollView>
    <Footer />
  </SafeAreaView>
);

const stackConfig = {
  defaultNavigationOptions: {
    header: null
  }
};

const Routes = createSwitchNavigator({
  Preload: {
    screen: Preload
  },
  Welcome: createStackNavigator(
    {
      Welcome: { screen: Welcome }
    },
    stackConfig
  ),
  Main: createDrawerNavigator(
    {
      Dashboard: {
        screen: Dashboard,
        navigationOptions: {
          drawerIcon: ({ tintColor }) => (
            <Icon name="home" size={18} color={tintColor} />
          ),
          title: "Início"
        }
      },
      Empresas: createStackNavigator(
        {
          Empresas: {
            screen: Empresas
          },
          EnterpriseScreen: {
            screen: EnterpriseScreen
          }
        },
        {
          ...stackConfig,
          navigationOptions: {
            drawerIcon: ({ tintColor }) => (
              <Icon name="city" size={18} color={tintColor} />
            ),
            title: "Empresas"
          }
        }
      )
    },
    {
      contentComponent: DrawerContent,
      contentOptions: {
        activeTintColor: "#002966",
        inactiveTintColor: "#00000060",
        itemStyle: {
          justifyContent: "flex-start",
          borderBottomWidth: 1,
          borderBottomColor: "#FFFFFF10"
        },
        labelStyle: {
          marginLeft: -5,
          marginBottom: 10,
          fontFamily: "Poppins Regular",
          fontWeight: "normal",
          fontSize: 16
        }
      },
      unmountInactiveRoutes: true,
      drawerBackgroundColor: "#F2F2F2",
      overlayColor: "#00000070"
    }
  )
});

export default createAppContainer(Routes);
