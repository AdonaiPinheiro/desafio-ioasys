import React, { Component } from "react";

// Redux
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";

// Reducers
import Reducers from "./assets/store";

// Criando store
let store = createStore(Reducers, applyMiddleware(ReduxThunk));

// Configuração da StatusBar para todo o App
import "./configs/global/statusBar";

// Desabilitando a YellowBox
console.disableYellowBox = true;

// Importação da Aplicação inteira que está contida em "Routes"
import Routes from "./routes";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Routes />
      </Provider>
    );
  }
}
