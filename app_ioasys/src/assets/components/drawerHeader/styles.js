import { StyleSheet } from "react-native";
import { metrics, colors } from "../../../configs/global/styles";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 150,
    justifyContent: "center",
    alignItems: "center"
  },
  profileImg: {
    marginTop: 5,
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: colors.white,
    justifyContent: "center",
    alignItems: "center"
  },
  welcomeText: {
    ...metrics.textRegular,
    marginTop: 5,
    fontSize: 16,
    color: colors.white
  }
});

export default styles;
