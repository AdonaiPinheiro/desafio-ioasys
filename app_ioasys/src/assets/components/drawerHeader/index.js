import React, { Component } from "react";
import { View, Text, Image } from "react-native";

// Linear Component
import LinearGradient from "react-native-linear-gradient";

// Icon
import Icon from "react-native-vector-icons/FontAwesome5";

// Redux
import { connect } from "react-redux";

// Styles
import styles from "./styles";

export class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let p = this.props;

    return (
      <LinearGradient
        start={{ x: 0, y: 1 }}
        end={{ x: 1, y: 0 }}
        colors={["#002966", "#001433"]}
        style={styles.container}
      >
        {p.userData.investor.photo !== null ? (
          <Image
            style={styles.profileImg}
            source={{ uri: p.userData.investor.photo }}
          />
        ) : (
          <View style={styles.profileImg}>
            <Icon name="user" color="#002966" size={24} />
          </View>
        )}

        <Text style={styles.welcomeText}>
          Bem-vindo, {p.userData.investor.investor_name}
        </Text>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return {
    userData: state.Auth.userData
  };
};

const HeaderConnect = connect(
  mapStateToProps,
  {}
)(Header);

export default HeaderConnect;
