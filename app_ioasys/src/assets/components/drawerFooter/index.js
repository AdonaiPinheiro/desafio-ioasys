import React, { Component } from "react";
import { TouchableOpacity, Text } from "react-native";
import { withNavigation } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";

// Icon
import Icon from "react-native-vector-icons/FontAwesome5";

// Redux
import { connect } from "react-redux";

// Styles
import styles from "./styles";

export class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <TouchableOpacity
        onPress={async () => {
          await AsyncStorage.clear();
          this.props.navigation.navigate("Preload");
        }}
        activeOpacity={0.6}
        style={styles.container}
      >
        <Icon name="door-open" color="#ff1a1a" solid size={18} />
        <Text style={styles.exitText}>Sair</Text>
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = state => {
  return {
    userData: state.Auth.userData
  };
};

const FooterConnect = connect(
  mapStateToProps,
  {}
)(withNavigation(Footer));

export default FooterConnect;
