import { StyleSheet } from "react-native";
import { metrics, colors } from "../../../configs/global/styles";

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: 60,
    width: "100%",
    alignItems: "center",
    marginLeft: 20
  },
  exitText: {
    marginLeft: 10,
    marginBottom: -5,
    ...metrics.textRegular,
    fontSize: 16,
    color: "#ff1a1a"
  }
});

export default styles;
