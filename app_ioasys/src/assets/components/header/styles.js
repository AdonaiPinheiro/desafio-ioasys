import { StyleSheet } from "react-native";
import { metrics, colors } from "../../../configs/global/styles";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    paddingVertical: 10,
    paddingHorizontal: 20,
    justifyContent: "space-between"
  },
  headerButton: {
    width: 50,
    height: 50,
    justifyContent: "center"
  },
  logo: {
    width: 541 * 0.1,
    height: 426 * 0.1
  }
});

export default styles;
