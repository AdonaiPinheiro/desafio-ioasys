import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { withNavigation } from "react-navigation";

// Styles
import styles from "./styles";

// Icons
import Icon from "react-native-vector-icons/FontAwesome5";

// Images
import { logoBlue, logoWhite } from "../../images";

export class Header extends Component {
  render() {
    let p = this.props;

    return (
      <View style={styles.container}>
        <TouchableOpacity
          hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
          activeOpacity={0.6}
          onPress={() => {
            p.back
              ? this.props.navigation.goBack(null)
              : this.props.navigation.openDrawer();
          }}
          style={styles.headerButton}
        >
          <Icon
            name={p.back ? "arrow-left" : "bars"}
            color={p.back ? "#F2F2F2" : "#002966"}
            size={24}
          />
        </TouchableOpacity>
        <Image source={p.back ? logoWhite : logoBlue} style={styles.logo} />
        <View style={styles.headerButton} />
      </View>
    );
  }
}

export default withNavigation(Header);
