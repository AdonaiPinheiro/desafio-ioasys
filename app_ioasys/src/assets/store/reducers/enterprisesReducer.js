const initialState = {
  enterprises: []
};

const Auth = (state = [], action) => {
  if (state.length == 0) {
    return initialState;
  }

  if (action.type == "setEnterprises") {
    return { ...state, enterprises: action.payload.enterprises };
  }

  return state;
};

export default Auth;
