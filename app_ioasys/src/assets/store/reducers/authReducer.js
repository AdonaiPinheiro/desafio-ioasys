const initialState = {
  email: "testeapple@ioasys.com.br",
  pass: "12341234",
  headers: { uid: "", client: "", accessToken: "" },
  userData: {}
};

const Auth = (state = [], action) => {
  if (state.length == 0) {
    return initialState;
  }

  if (action.type == "editEmail") {
    return { ...state, email: action.payload.email };
  }

  if (action.type == "editPass") {
    return { ...state, pass: action.payload.pass };
  }

  if (action.type == "editHeaders") {
    return { ...state, headers: action.payload.headers };
  }

  if (action.type == "editUserData") {
    return { ...state, userData: action.payload.userData };
  }

  return state;
};

export default Auth;
