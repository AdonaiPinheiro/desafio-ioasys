export const editEmail = email => {
  return {
    type: "editEmail",
    payload: {
      email: email
    }
  };
};

export const editPass = pass => {
  return {
    type: "editPass",
    payload: {
      pass: pass
    }
  };
};

export const editHeaders = (uid, client, accessToken) => {
  return {
    type: "editHeaders",
    payload: {
      headers: { uid, client, accessToken }
    }
  };
};

export const editUserData = data => {
  return {
    type: "editUserData",
    payload: {
      userData: data
    }
  };
};
