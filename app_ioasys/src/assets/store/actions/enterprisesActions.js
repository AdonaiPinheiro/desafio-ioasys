export const setEnterprises = enterprises => {
  return {
    type: "setEnterprises",
    payload: {
      enterprises: enterprises
    }
  };
};
