import { combineReducers } from "redux";

// Reducers
import Auth from "./reducers/authReducer";
import Enterprises from "./reducers/enterprisesReducer";

const Reducers = combineReducers({
  Auth: Auth,
  Enterprises: Enterprises
});

export default Reducers;
