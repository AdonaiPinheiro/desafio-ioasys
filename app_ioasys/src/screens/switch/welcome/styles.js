import { StyleSheet, Dimensions } from "react-native";
import { metrics, colors } from "../../../configs/global/styles";

const { width, height } = Dimensions.get("screen");

const styles = StyleSheet.create({
  container: {
    ...metrics.container,
    backgroundColor: colors.white,
    justifyContent: "space-around",
    alignItems: "center"
  },
  content: { justifyContent: "center", alignItems: "center" },
  fundo: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0
  },
  iconImg: {
    position: "absolute",
    zIndex: 1,
    width: 541 * 0.15,
    height: 426 * 0.15,
    marginBottom: 20
  },
  iconImgBack: {
    zIndex: 0,
    marginLeft: 3,
    marginTop: 3,
    width: 541 * 0.15,
    height: 426 * 0.15,
    marginBottom: 20
  },
  textInput: {
    backgroundColor: "#FFF",
    padding: 10,
    margin: 10,
    height: 40,
    width: width - 120,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: colors.primary,
    ...metrics.textRegular,
    paddingBottom: 4,
    paddingHorizontal: 15,
    textAlign: "center"
  },
  title: {
    ...metrics.textBold,
    color: "#FFF",
    fontSize: 24
  },
  button: {
    backgroundColor: colors.primaryDark,
    height: 40,
    margin: 10,
    width: width - 120,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: colors.white
  },
  buttonText: {
    ...metrics.textRegular,
    color: "#FFF",
    fontSize: 16,
    marginBottom: -5
  }
});

export default styles;
