import React, { Component } from "react";
import {
  Alert,
  View,
  Text,
  Image,
  Animated,
  TextInput,
  TouchableOpacity
} from "react-native";

// AsyncStorage
import AsyncStorage from "@react-native-community/async-storage";

// Redux
import { connect } from "react-redux";
import {
  editEmail,
  editPass,
  editHeaders,
  editUserData
} from "../../../assets/store/actions/authActions";

// Images
import { fundoHeader, logoWhite, logoBlue } from "../../../assets/images";
import styles from "./styles";

// Api
import api from "../../../services/api";

export class Welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fundoOpacity: new Animated.Value(0),
      iconOpacity: new Animated.Value(0),
      titleOpacity: new Animated.Value(0),
      emailOpacity: new Animated.Value(0),
      passOpacity: new Animated.Value(0),
      buttonOpacity: new Animated.Value(0),

      // Button
      loading: false
    };
  }

  componentDidMount() {
    this.animations();
  }

  animations = () => {
    let s = this.state;

    Animated.sequence([
      Animated.timing(s.fundoOpacity, {
        toValue: 1,
        duration: 500
      }),
      Animated.timing(s.iconOpacity, {
        toValue: 1,
        duration: 1000
      }),
      Animated.timing(s.titleOpacity, {
        toValue: 1,
        duration: 500
      }),
      Animated.timing(s.emailOpacity, {
        toValue: 1,
        duration: 500
      }),
      Animated.timing(s.passOpacity, {
        toValue: 1,
        duration: 500
      }),
      Animated.timing(s.buttonOpacity, {
        toValue: 1,
        duration: 500
      })
    ]).start();
  };

  login = () => {
    let s = this.state;
    let p = this.props;
    s.loading = true;
    this.setState(s);

    api
      .login(p.email, p.pass)
      .then(async r => {
        console.log(r.data);
        let uid = r.headers.uid;
        let client = r.headers.client;
        let accToken = r.headers["access-token"];

        await p.editHeaders(uid, client, accToken);
        await p.editUserData(r.data);
        await AsyncStorage.multiSet([
          ["@userUid", uid],
          ["@userClient", client],
          ["@userAccessToken", accToken],
          ["@userData", JSON.stringify(r.data)]
        ]);

        s.loading = false;
        this.setState(s);

        this.props.navigation.navigate("Dashboard");
      })
      .catch(e => {
        console.log(e.message);
        s.loading = false;
        this.setState(s);
        Alert.alert("Atenção", "E-mail ou senha incorretos");
      });
  };

  render() {
    let s = this.state;
    let p = this.props;

    return (
      <View style={styles.container}>
        <Animated.Image
          source={fundoHeader}
          style={[
            styles.fundo,
            {
              opacity: s.fundoOpacity
            }
          ]}
        />

        <Animated.View
          style={{
            marginTop: 100,
            opacity: this.state.iconOpacity
          }}
        >
          <Image source={logoWhite} style={styles.iconImg} />
          <Image source={logoBlue} style={styles.iconImgBack} />
        </Animated.View>

        <View style={styles.content}>
          <Animated.View style={{ opacity: s.titleOpacity }}>
            <Text style={styles.title}>Bem-vindo a ioasys!</Text>
          </Animated.View>
          <Animated.View style={{ opacity: s.emailOpacity }}>
            <TextInput
              value={p.email}
              onChangeText={text => {
                p.editEmail(text);
              }}
              keyboardType="email-address"
              style={styles.textInput}
              placeholder="E-mail"
            />
          </Animated.View>
          <Animated.View style={{ opacity: s.passOpacity }}>
            <TextInput
              value={p.pass}
              onChangeText={text => {
                p.editPass(text);
              }}
              secureTextEntry={true}
              style={styles.textInput}
              placeholder="Senha"
            />
          </Animated.View>
          <Animated.View style={{ opacity: s.buttonOpacity }}>
            <TouchableOpacity
              disabled={s.loading}
              onPress={this.login}
              activeOpacity={0.6}
              style={styles.button}
            >
              <Text style={styles.buttonText}>
                {s.loading ? "Carregando..." : "Entrar"}
              </Text>
            </TouchableOpacity>
          </Animated.View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    email: state.Auth.email,
    pass: state.Auth.pass
  };
};

const WelcomeConnect = connect(
  mapStateToProps,
  { editEmail, editPass, editHeaders, editUserData }
)(Welcome);

export default WelcomeConnect;
