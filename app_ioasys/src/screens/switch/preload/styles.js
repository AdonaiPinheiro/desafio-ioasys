import { StyleSheet } from "react-native";
import { metrics, colors } from "../../../configs/global/styles";

const styles = StyleSheet.create({
  container: {
    ...metrics.container,
    justifyContent: "center",
    alignItems: "center"
  },
  img: {
    width: 541 * 0.15,
    height: 426 * 0.15,
    marginBottom: 20
  }
});

export default styles;
