import React, { Component } from "react";
import { Image, View, ActivityIndicator } from "react-native";

// AsyncStorage
import AsyncStorage from "@react-native-community/async-storage";

// Redux
import { connect } from "react-redux";
import {
  editHeaders,
  editUserData
} from "../../../assets/store/actions/authActions";

// Api
import api from "../../../services/api";

// Gradient Component
import LinearGradient from "react-native-linear-gradient";
const colors = ["#002966", "#0047b3"];

// Styles
import styles from "./styles";

// Images
import { logoWhite } from "../../../assets/images";

export class Preload extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.checkToken();
  }

  checkToken = async () => {
    let p = this.props;

    let userHeaders = await AsyncStorage.multiGet([
      "@userUid",
      "@userClient",
      "@userAccessToken"
    ]);

    let userData = await AsyncStorage.getItem("@userData");

    p.editHeaders(userHeaders[0][1], userHeaders[1][1], userHeaders[2][1]);

    p.editUserData(JSON.parse(userData));

    let headers = {
      uid: userHeaders[0][1],
      client: userHeaders[1][1],
      "access-token": userHeaders[2][1]
    };

    api
      .checkToken(headers)
      .then(r => {
        this.props.navigation.navigate("Dashboard");
      })
      .catch(e => {
        this.props.navigation.navigate("Welcome");
      });
  };

  render() {
    return (
      <LinearGradient colors={colors} style={styles.container}>
        <Image source={logoWhite} style={styles.img} />
        <ActivityIndicator size="large" color="#f2f2f2" />
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return {
    headers: state.Auth.headers
  };
};

const PreloadConnect = connect(
  mapStateToProps,
  { editHeaders, editUserData }
)(Preload);

export default PreloadConnect;
