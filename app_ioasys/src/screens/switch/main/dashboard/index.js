import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, ScrollView } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import AsyncStorage from "@react-native-community/async-storage";

// Styles
import styles from "./styles";

// Redux
import { connect } from "react-redux";

// Icons
import Icon from "react-native-vector-icons/FontAwesome5";

// Api
import api from "../../../../services/api";

// Components
import Header from "../../../../assets/components/header";

// countries
import countries from "../../../../assets/content/countries/countries.json";

export class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flag: "",
      lines: 5
    };
  }

  componentDidMount() {
    this.getFlag();
  }

  getFlag = () => {
    let p = this.props.userData.investor;
    let s = this.state;

    countries.forEach(r => {
      if (r.nome_pais === p.country) {
        s.flag = r.sigla.toLowerCase();
        this.setState(s);
      }
    });
  };

  perfil = () => {
    this.props.navigation.openDrawer();
  };

  empresas = () => {
    this.props.navigation.navigate("Empresas");
  };

  logout = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate("Preload");
  };

  render() {
    let s = this.state;
    let p = this.props.userData;

    return (
      <View style={styles.container}>
        <Header />
        <View style={styles.content}>
          <View style={styles.profile}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "flex-end"
              }}
            >
              <View>
                <Text style={styles.name}>{p.investor.investor_name}</Text>
                <Text style={styles.email}>{p.investor.email}</Text>
              </View>

              <Image
                source={{
                  uri: `https://www.bandeirasnacionais.com/data/flags/w580/${s.flag}.png`
                }}
                style={styles.flag}
              />
            </View>
          </View>
          <View style={styles.resume}>
            <Text style={styles.resumeTitle}>Resumo Diário</Text>
            <ScrollView>
              <Text numberOfLines={s.lines} style={styles.resumeText}>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum.
              </Text>
            </ScrollView>
            <TouchableOpacity
              onPress={() => {
                this.setState({ lines: s.lines === 5 ? 20 : 5 });
              }}
              activeOpacity={0.6}
            >
              <Text style={styles.resumeReadMore}>
                {s.lines === 5 ? "Continue lendo" : "Recolher"}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.nav}>
            <TouchableOpacity
              onPress={this.perfil}
              activeOpacity={0.6}
              style={styles.buttonsNav}
            >
              <Icon name="user" color="#F2F2F2" size={24} />
              <Text style={styles.navText}>Perfil</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.empresas}
              activeOpacity={0.6}
              style={styles.buttonsNav}
            >
              <Icon name="building" light color="#F2F2F2" size={24} />
              <Text style={[styles.navText, { fontSize: 13 }]}>Empresas</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.6} style={styles.buttonsNav}>
              <Icon name="feather-alt" color="#F2F2F2" size={24} />
              <Text style={styles.navText}>Outros</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.logout}
              activeOpacity={0.6}
              style={styles.buttonsNav}
            >
              <Icon name="door-open" color="#ff1a1a" size={24} />
              <Text style={[styles.navText, { color: "#ff1a1a" }]}>Sair</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    headers: state.Auth.headers,
    userData: state.Auth.userData
  };
};

const DashboardConnect = connect(
  mapStateToProps,
  {}
)(Dashboard);

export default DashboardConnect;
