import { StyleSheet, Dimensions } from "react-native";
import { metrics, colors } from "../../../../configs/global/styles";

const styles = StyleSheet.create({
  container: {
    ...metrics.container
  },
  content: {
    flex: 1
  },
  profile: {
    borderRadius: 10,
    padding: 10,
    paddingLeft: 15,
    width: "100%",
    backgroundColor: "#002966"
  },
  backFooter: {
    position: "absolute",
    zIndex: 0,
    left: 0,
    right: 0,
    bottom: 0,
    height: 200
  },

  // User
  flag: {
    width: 285 * 0.15,
    height: 200 * 0.15,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: "#F2F2F2"
  },
  name: {
    ...metrics.textSemiBold,
    color: colors.white,
    fontSize: 18,
    marginBottom: -5
  },
  email: {
    ...metrics.textRegular,
    color: colors.white,
    fontSize: 16
  },
  resume: {
    width: "100%",
    padding: 10
  },
  resumeTitle: {
    ...metrics.textSemiBold,
    color: "#001433",
    fontSize: 16
  },
  resumeText: {
    ...metrics.textRegular,
    color: "#001433",
    textAlign: "left"
  },
  resumeReadMore: {
    ...metrics.textSemiBold,
    color: "#00143375",
    fontSize: 14,
    textAlign: "right"
  },
  nav: {
    position: "absolute",
    flexDirection: "row",
    justifyContent: "space-between",
    zIndex: 1,
    bottom: 0,
    left: 0,
    right: 0,
    padding: 10
  },
  buttonsNav: {
    height: 100,
    width: "23.5%",
    borderBottomWidth: 2,
    borderRightWidth: 2,
    borderColor: "#ccc",
    backgroundColor: colors.primary,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5
  },
  navText: {
    ...metrics.textRegular,
    color: colors.white,
    marginTop: 5,
    marginBottom: -5
  }
});

export default styles;
