import { StyleSheet } from "react-native";
import { metrics, colors } from "../../../../configs/global/styles";

const styles = StyleSheet.create({
  container: {
    ...metrics.container
  },
  search: {
    width: "90%",
    marginRight: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    paddingTop: 10,
    backgroundColor: "#FFF",
    borderRadius: 20,
    borderWidth: 0.5,
    borderColor: "#CCC",
    ...metrics.textRegular,
    fontSize: 14,
    marginBottom: 10
  },

  // FlatList
  emptyList: {
    flex: 1,
    width: "100%",
    height: 500,
    justifyContent: "center",
    alignItems: "center"
  },
  emptyText: {
    marginTop: 10,
    color: colors.primaryDark,
    ...metrics.textRegular,
    fontSize: 16
  },

  buttonRefresh: {
    padding: 10,
    paddingVertical: 7.5,
    marginTop: 10,
    backgroundColor: colors.primary,
    borderRadius: 5
  },
  buttonText: {
    color: colors.white,
    ...metrics.textRegular,
    fontSize: 16,
    marginBottom: -5
  }
});

export default styles;
