import { StyleSheet, Dimensions } from "react-native";
import { metrics, colors } from "../../../../../configs/global/styles";

const { width } = Dimensions.get("screen");

const styles = StyleSheet.create({
  container: {
    ...metrics.container,
    padding: 0
  },
  header: {
    backgroundColor: colors.primary,
    paddingTop: 7.5,
    justifyContent: "center",
    alignItems: "center"
  },
  info: {
    padding: 10,
    width: width,
    height: 250,
    backgroundColor: colors.primary,
    justifyContent: "center"
  },
  infoTitle: {
    ...metrics.textSemiBold,
    marginBottom: -5,
    fontSize: 28,
    color: colors.white,
    textAlign: "center"
  },
  infoDesc: {
    ...metrics.textRegular,
    marginBottom: 20,
    fontSize: 14,
    color: colors.white,
    textAlign: "center"
  },
  descTitle: {
    ...metrics.textSemiBold,
    fontSize: 16
  },
  descText: {
    ...metrics.textRegular,
    fontSize: 16,
    padding: 10,
    borderWidth: 0.5,
    borderRadius: 5,
    borderColor: "#001433"
  }
});

export default styles;
