import React, { Component } from "react";
import { View, Text, TouchableOpacity, ScrollView, Image } from "react-native";

// Icon
import Icon from "react-native-vector-icons/FontAwesome5";

// Components
import Header from "../../../../../assets/components/header";

// Styles
import styles from "./styles";

export default class EnterpriseScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let p = this.props.navigation.state.params.data;

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Header back={true} />
        </View>
        <ScrollView>
          <View style={styles.info}>
            <Image
              style={{
                position: "absolute",
                top: 0,
                bottom: 0,
                right: 0,
                left: 0,
                opacity: 0.3
              }}
              source={{ uri: `http://empresas.ioasys.com.br${p.photo}` }}
            />
            <View>
              <Text style={styles.infoTitle}>{p.enterprise_name}</Text>
            </View>
            <View>
              <Text style={styles.infoDesc}>
                {p.enterprise_type.enterprise_type_name} | {p.city} |{" "}
                {p.country}
              </Text>
            </View>
          </View>
          <View style={{ padding: 10 }}>
            <Text style={styles.descTitle}>Descrição</Text>
            <Text style={styles.descText}>{p.description}</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignSelf: "flex-end",
              justifyContent: "space-between",
              padding: 10,
              width: 120
            }}
          >
            <TouchableOpacity>
              <Icon
                name="linkedin"
                color={
                  p.linkedin !== "" && p.linkedin !== null ? "#0e76a8" : "#CCC"
                }
                size={32}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon
                name="facebook-square"
                color={
                  p.facebook !== "" && p.facebook !== null ? "#3b5998" : "#CCC"
                }
                size={32}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon
                name="twitter-square"
                color={
                  p.twitter !== "" && p.twitter !== null ? "#00acee" : "#CCC"
                }
                size={32}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
