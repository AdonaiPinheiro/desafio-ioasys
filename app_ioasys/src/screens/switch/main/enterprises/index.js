import React, { Component } from "react";
import {
  ActivityIndicator,
  View,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  Keyboard
} from "react-native";

// Redux
import { connect } from "react-redux";
import { setEnterprises } from "../../../../assets/store/actions/enterprisesActions";

// Api
import api from "../../../../services/api";

// Styles
import styles from "./styles";

// Icons
import Icon from "react-native-vector-icons/FontAwesome5";

// Components
import Header from "../../../../assets/components/header";
import EnterpriseFlatItem from "./enterpriseFlatItem";

export class Empresas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      search: ""
    };
  }

  componentDidMount() {
    this.getEnterprises();
  }

  getEnterprises = () => {
    let p = this.props;
    let s = this.state;
    this.setState({ loader: true });

    let headers = {
      uid: p.headers.uid,
      client: p.headers.client,
      "access-token": p.headers.accessToken
    };

    api
      .getEnterprises(headers)
      .then(r => {
        p.setEnterprises(r.data.enterprises);
        this.setState({ loader: false });
        console.log(r.data.enterprises);
      })
      .catch(e => {
        console.log(e);
        this.setState({ loader: false });
        this.props.navigation.navigate("Welcome");
      });
  };

  search = text => {
    let p = this.props;
    let s = this.state;
    this.setState({ loader: true });

    let headers = {
      uid: p.headers.uid,
      client: p.headers.client,
      "access-token": p.headers.accessToken
    };

    api
      .search(headers, text)
      .then(r => {
        p.setEnterprises(r.data.enterprises);
        this.setState({ loader: false });
        console.log(r.data.enterprises);
      })
      .catch(e => {
        console.log(e);
        this.setState({ loader: false });
        this.props.navigation.navigate("Welcome");
      });
  };

  render() {
    let s = this.state;
    let p = this.props;

    return (
      <View style={styles.container}>
        <Header />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <TextInput
            value={s.search}
            onChangeText={text => {
              this.setState({ search: text });
              this.search(text);
            }}
            style={styles.search}
            placeholder="Pesquisar"
          />
          <TouchableOpacity
            onPress={() => {
              Keyboard.dismiss();
              this.getEnterprises();
              this.setState({ search: "" });
            }}
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: -7.5
            }}
          >
            <Icon name="times" size={28} color="#a6a6a6" />
          </TouchableOpacity>
        </View>
        <FlatList
          data={p.enterprises}
          renderItem={({ item }) => <EnterpriseFlatItem data={item} />}
          style={{
            flex: 1
          }}
          keyExtractor={item => item.id.toString()}
          ListEmptyComponent={
            s.loader ? (
              <View style={styles.emptyList}>
                <ActivityIndicator size="large" color="#001433" />
              </View>
            ) : (
              <View style={styles.emptyList}>
                <Icon name="frown-open" size={36} color="#001433" solid />
                <Text style={styles.emptyText}>
                  Não há empresas disponíveis
                </Text>
                <TouchableOpacity
                  onPress={this.getEnterprises}
                  activeOpacity={0.6}
                  style={styles.buttonRefresh}
                >
                  <Text style={styles.buttonText}>Atualizar</Text>
                </TouchableOpacity>
              </View>
            )
          }
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    headers: state.Auth.headers,
    enterprises: state.Enterprises.enterprises
  };
};

const EmpresasConnect = connect(
  mapStateToProps,
  { setEnterprises }
)(Empresas);

export default EmpresasConnect;
