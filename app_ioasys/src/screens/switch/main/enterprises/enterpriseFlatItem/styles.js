import { StyleSheet } from "react-native";
import { metrics, colors } from "../../../../../configs/global/styles";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 100,
    zIndex: 0,
    borderRadius: 5,
    backgroundColor: "#FFF",
    marginVertical: 5
  },
  content: {
    padding: 10,
    flex: 1,
    zIndex: 7,
    flexDirection: "row"
  },
  title: {
    ...metrics.textSemiBold,
    color: colors.white,
    marginBottom: -5,
    fontSize: 18
  },
  city: {
    ...metrics.textSemiBold,
    color: colors.white,
    fontSize: 14,
    marginTop: -5
  },
  description: {
    ...metrics.textRegular,
    color: colors.white,
    marginBottom: 0,
    fontSize: 14,
    width: 100
  },
  backGradient: {
    position: "absolute",
    zIndex: 2,
    left: 0,
    top: 0,
    bottom: 0,
    width: "100%",
    borderRadius: 5
  },
  flag: {
    position: "absolute",
    right: 0,
    left: 0,
    top: 0,
    bottom: 0,
    zIndex: 1,
    width: "100%",
    height: "100%",
    borderRadius: 5,
    borderWidth: 2,
    borderColor: "#F2F2F2"
  },
  img: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: "#F2F2F2",
    justifyContent: "center",
    alignItems: "center"
  }
});

export default styles;
