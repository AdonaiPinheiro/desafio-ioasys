import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { withNavigation } from "react-navigation";
import LinearGradient from "react-native-linear-gradient";

// Styles
import styles from "./styles";

// Icon
import Icon from "react-native-vector-icons/FontAwesome5";

// countries
import countries from "../../../../../assets/content/countries/countries.json";

export class EnterpriseFlatItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flag: ""
    };
  }

  componentDidMount() {
    this.getFlag();
  }

  getFlag = () => {
    let p = this.props.data;
    let s = this.state;

    countries.forEach(r => {
      if (r.nome_pais === p.country) {
        s.flag = r.sigla.toLowerCase();
        this.setState(s);
      }
    });
  };

  render() {
    let s = this.state;
    let p = this.props.data;

    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate("EnterpriseScreen", { data: p });
        }}
        activeOpacity={0.7}
        style={styles.container}
      >
        <View style={styles.content}>
          <View style={styles.img}>
            {p.photo === null ? (
              <Icon name="users" size={28} color="#001433" />
            ) : (
              <Image
                style={styles.img}
                source={{ uri: `http://empresas.ioasys.com.br${p.photo}` }}
              />
            )}
          </View>
          <View style={{ marginLeft: 10, justifyContent: "space-between" }}>
            <View>
              <Text style={styles.title}>{p.enterprise_name}</Text>
              <Text style={styles.city}>{p.city}</Text>
            </View>
            <View style={{ padding: 5, backgroundColor: "#00000030" }}>
              <Text numberOfLines={1} style={styles.description}>
                {p.description}
              </Text>
            </View>
          </View>
        </View>
        <Image
          source={{
            uri: `https://www.bandeirasnacionais.com/data/flags/w580/${s.flag}.png`
          }}
          style={styles.flag}
        />

        <LinearGradient
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 0 }}
          style={styles.backGradient}
          colors={["#002966", "#FFFFFF50"]}
        />
      </TouchableOpacity>
    );
  }
}

export default withNavigation(EnterpriseFlatItem);
