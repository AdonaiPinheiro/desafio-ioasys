import api from "./apiConnection";

class Api {
  /* Função para pegar versão armazenada no Redux, enviar como
  parametro e verificar se bate com a versão do server */
  async login(email, pass) {
    let formData = new FormData();
    formData.append("email", email);
    formData.append("password", pass);

    return await api.post(`/users/auth/sign_in`, formData, {
      headers: { "Content-Type": "application/form-data" }
    });
  }

  // Estou somente verificando se consigo realizar um get com as mesmas credenciais, não precisa de retorno
  async checkToken(headers) {
    await api.get(`/enterprises`, { headers: headers });
  }

  // Função que retornará todas as empresas cadastradas
  async getEnterprises(headers) {
    return await api.get(`/enterprises`, { headers: headers });
  }

  // Função search
  async search(headers, enterprise) {
    return await api.get(`/enterprises?name=${enterprise}`, {
      headers: headers
    });
  }
}

export default new Api();
