import { StatusBar } from "react-native";

StatusBar.setBackgroundColor("#002966");
StatusBar.setBarStyle("light-content");
StatusBar.setHidden(false);
