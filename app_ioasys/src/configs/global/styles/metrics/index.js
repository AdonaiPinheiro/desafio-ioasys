import { Dimensions, StyleSheet } from "react-native";
import colors from "../colors";

const { width, height } = Dimensions.get("window");

const metrics = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    width: width,
    height: height,
    backgroundColor: colors.white
  },

  // Fontes utilizadas no sistema
  textRegular: {
    fontFamily: "Poppins Regular"
  },
  textBold: {
    fontFamily: "Poppins Bold"
  },
  textSemiBold: {
    fontFamily: "Poppins SemiBold"
  },
  textItalic: {
    fontFamily: "Poppins Italic"
  },

  // Define o tamanho real da logo,
  // que deverá ser reduzida de acordo com a necessidade.
  logoSize: {
    width: 632,
    height: 446
  },
  iconSize: {
    width: 284,
    height: 192
  }
});

export default metrics;
