export default {
  white: "#F2F2F2",
  lighter: "#EEE",
  light: "#DDD",
  regular: "#999",
  dark: "#666",
  darker: "#333",
  black: "#000",

  primary: "#002966",
  primaryDark: "#001433",

  success: "#9DCA83",
  successDark: "#223617",

  warn: "#fdce35",
  warnDark: "#322701",

  // Alert colors
  danger: "#E37A7A",
  dangerDark: "#3f0d0d",

  transparent: "transparent",
  darkTransparent: "rgba(0, 0, 0, 0.6)",
  whiteTransparent: "rgba(255, 255, 255, 0.3)"
};
